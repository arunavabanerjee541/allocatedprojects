﻿namespace EmployeeDataManagement
{
    public enum EmployeeType
    {
        PermanentEmployee,
        ContractualEmployee
    }
}
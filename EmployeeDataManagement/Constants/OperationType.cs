﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDataManagement
{
      public enum OperationType
      {
        AddAnEmployee,
        GetDetailsOfAllEmployees,
        UpdateAnEmployee,
        DeleteEmployeeByEmail,
        DeleteEmployeeByName,
        GetEmployeeByEmailAddress,
        GetTotalSalary

      }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDataManagement
{
    public delegate bool CheckData(string data, List<Employee> employeeList);
    
    interface IValidate
    {
     bool IsDataValid(string mail ,CheckData data,List<Employee> employeeList);
    }
}

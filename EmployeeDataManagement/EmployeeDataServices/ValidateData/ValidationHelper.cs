﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDataManagement
{
    class ValidationHelper
    {

        public bool IsContentOfTheMailValid(string mail, List<Employee> employeeList)
        {
            MailAddress mailAddress;
            try
            {
                mailAddress = new MailAddress(mail);
            }
            catch (Exception)
            {
                Console.WriteLine("INVALID MAIL FORMAT ");
                return false;
            }
            string address = mailAddress.Address;

            if(CountOfDotAppreance(address)>1) return false ;
            if(CountOfAtAppreance(address)>1) return false;
            if (!AreSomeSpecialValidationsObeyed(address)) return false;
            if (address.Length < 3 || address.Length > 40 || !address.Contains(".")) return false;
           
            return true;

        }


        public bool IsContentOfNameValid(string name, List<Employee> employeeList)
        {
            if (name.Length < 2 || name.Length > 20 || name.Any(x => !char.IsLetter(x)))
            {
                return false;
            }
            return true;

        }

        public bool DoesMailAddressExist(string mail, List<Employee> employeeList)
        {
            if (IsContentOfTheMailValid(mail, employeeList))
            {
                var employee = employeeList.Where(empl => empl.GetEmailAddress().Equals(mail));

                if (employee.Count() == 1)
                    return true;
            }
            return false;
        }



        public bool DoesEmployeeWithThisNameExists(string firstName, List<Employee> employeeList)
        {
          if (IsContentOfNameValid(firstName, employeeList)&& DoesNameExists(firstName,employeeList))   
                return true;

            return false;
        }

      
        public bool AreSomeSpecialValidationsObeyed(string address)
        {
            if (!IsAllValidCharacters(address)) return false;
            if (!IsFirstTwoCharactersValid(address.Substring(0, 2))) return false;
            if (DoesLettersAfterDotHasDigit(address)) return false;
            if (address.IndexOf('.') < address.IndexOf('@')) return false;

            return true;

        }


        public bool IsFirstTwoCharactersValid(string twoCharacters)
        {
            return twoCharacters.All(ch => char.IsLetter(ch));
        }


        public bool IsAllValidCharacters(string address)
        {
            return address.All(ch => char.IsLetter(ch)
            || char.IsDigit(ch) || ch == '@' || ch == '.');
        }

        public bool DoesLettersAfterDotHasDigit(string address)
        {
            return address.Substring(address.IndexOf('@') + 1).Any(ch => char.IsDigit(ch));
        }

        public bool DoesNameExists(string firstName, List<Employee> employeeList)
        {
            return employeeList.Any(emp => emp.FirstName.Equals(firstName));
        }


        public int CountOfDotAppreance(string address)
        {
            return address.Where(character => character == '.').Count();
        }

        public int CountOfAtAppreance(string address)
        {
            return address.Where(character => character == '@').Count();
        }


    }


}

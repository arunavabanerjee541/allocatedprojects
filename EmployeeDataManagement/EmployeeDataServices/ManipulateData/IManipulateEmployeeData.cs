﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDataManagement
{
    interface IManipulateEmployeeData
    {
         void AddEmployeeData(List<Employee> employeeList);

         void UpdateEmployeeData(List<Employee> employeeList);

         void DeleteAnEmployee(MailAddress mail, List<Employee> employeeList);

         void DeleteAnEmployee(string firstName, List<Employee> employeeList);


    }
}

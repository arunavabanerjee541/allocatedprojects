﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDataManagement
{
    class ManipulateEmployeeData : IManipulateEmployeeData
    {
        private IValidate _validate;
        private ValidationHelper _validationHelper;

        public ManipulateEmployeeData()
        {
            _validate = new Validate();
            _validationHelper = new ValidationHelper();
        }

        public  void AddEmployeeData(List<Employee> employeeList)
        {
            Employee employee = BuildEmployeeData(employeeList);
            employeeList.Add(employee);

        }

        public void UpdateEmployeeData(List<Employee> employeeList)
        {
            Console.WriteLine( "Enter emloyee Mail Address" );
            string mail = Console.ReadLine();
            if(_validate.IsDataValid(mail, _validationHelper.DoesMailAddressExist,employeeList))
            {
                 MailAddress myMail = new MailAddress(mail);
                 var employee = employeeList.Where(myEmployee => myEmployee.GetEmailAddress().Equals(myMail)).Single();

                 Console.WriteLine("Please enter first name to update,Existing name is "+ employee.FirstName);
                 string updatedFirstname = GetName(employeeList);

                Console.WriteLine("Please enter last name to update,Existing name is " + employee.LastName);
                string updatedLastname = GetName(employeeList);

                Console.WriteLine("Please enter Gender to update,Existing Gender is " + employee.GetGender());
                Gender updatedGender = GetGender();

                Console.WriteLine("Please enter age to update,Existing age is " + employee.Age);
                int updatedAge = GetAge();

                Console.WriteLine("Please enter salary to update,Existing salary is " + employee.Salary);
                decimal updatedSalary = GetSalary();

                Console.WriteLine("Please enter Employee Type to update,Existing Employee Type is " + employee.GetEmployeeType());
                EmployeeType updatedEmployeeType = GetEmployeeType();

                if(updatedEmployeeType != employee.GetEmployeeType())
                {
                    employeeList.Remove(employee);
                    if (updatedEmployeeType == EmployeeType.PermanentEmployee)
                    {
                        employeeList.Add(new PermanentEmployee(updatedFirstname, updatedLastname, updatedAge, updatedSalary, updatedGender, updatedEmployeeType, myMail));
                    }
                    else
                    {
                        employeeList.Add(new ContractualEmployee(updatedFirstname, updatedLastname, updatedAge, updatedSalary, updatedGender, updatedEmployeeType, myMail));

                    }
                    Console.WriteLine(" Employee Details Updated ");

                }
                else
                {
                    employee.FirstName = updatedFirstname;
                    employee.LastName = updatedLastname;
                    employee.SetGender(updatedGender);
                    employee.Age = updatedAge;
                    employee.Salary = updatedSalary;
                    Console.WriteLine(" Employee Details Updated ");

                }

            }

            else
            Console.WriteLine(" No Employee detail found ");

        }


        public void DeleteAnEmployee(MailAddress mail, List<Employee> employeeList)
        {
            var employee = employeeList.Where(emp => emp.GetEmailAddress().Equals(mail)).Single();
            employeeList.Remove(employee);
            Console.WriteLine("Employee details deleted ");
        }

        public void DeleteAnEmployee(string firstName, List<Employee> employeeList)
        {
            var employees = employeeList.Where(emp => emp.FirstName.Equals(firstName));

            foreach(var employee in employees.ToList())
            {
               employeeList.Remove(employee);
            }
            Console.WriteLine("Employee details deleted");

        }


        public Employee BuildEmployeeData(List<Employee> employeeList)
        {
            Employee employee;

            Console.WriteLine("Please Enter first name ");
            string firstName =GetName(employeeList);
            Console.WriteLine("Please Enter second name ");
            string lastName = GetName(employeeList);
            MailAddress mail = GetMailAddress(employeeList);
            Gender gender = GetGender();
            int age = GetAge();
            decimal salary = GetSalary();          
            EmployeeType typeOfEmployee = GetEmployeeType();
            if(typeOfEmployee == EmployeeType.PermanentEmployee)
            {
                employee = new PermanentEmployee(firstName, lastName,age, salary, gender, typeOfEmployee, mail);
            }
            else
            {
                employee = new ContractualEmployee(firstName, lastName, age, salary, gender, typeOfEmployee, mail);
            }
            Console.WriteLine(" Employee Details Added ");
            return employee;        

        }



        public MailAddress GetMailAddress(List<Employee> employeeList)
        {
            Console.WriteLine("Please enter Employee Email Address ");
            string mail = Console.ReadLine();          
           if( _validate.IsDataValid(mail, _validationHelper.IsContentOfTheMailValid,employeeList))
            {
                if (!_validate.IsDataValid(mail, _validationHelper.DoesMailAddressExist, employeeList))
                {
                    return new MailAddress(mail);
                }
                else
                {
                    Console.WriteLine(" This Mail Address Already Exists ");
                    return GetMailAddress(employeeList);
                }
            }
            else
            {
                Console.WriteLine(" invalid email address " );
                return GetMailAddress(employeeList);
            }           
        }


        public string GetName(List<Employee> employeeList)
        {           
             string name = Console.ReadLine();
         if (  _validate.IsDataValid(name, _validationHelper.IsContentOfNameValid, employeeList))
            {
                return name;
            }
            else
            {
                Console.WriteLine(" Invalid name try again ");
                return GetName(employeeList);
            }
        
        }



        public int GetAge()
        {
            Console.WriteLine(" Please enter Age ");
            int age = 0;

            try
            {
                age = Convert.ToInt32(Console.ReadLine());
                if(age < 10 || age > 80)
                {
                    throw new Exception();
                }
            }
            catch(Exception)
            {
                Console.WriteLine("Invalid age ");
                return GetAge();
            }

            return age;
            
        }


        public decimal GetSalary()
        {
            Console.WriteLine(" Please enter Salary ");
            decimal salary = 0;

            try
            {
                salary = Convert.ToDecimal(Console.ReadLine());
                if(salary<1)
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid Salary ");
                return GetSalary();
            }

            return salary;

        }


        public Gender GetGender()
        {
            Console.WriteLine("Please enter gender ");
            Console.WriteLine("Enter 0 for Male ");
            Console.WriteLine("Enter 1 for Female ");
            Console.WriteLine("Enter 2 for Other ");

            int choice = 0;
            try
            {
                choice = Convert.ToInt32(Console.ReadLine());
                if(choice <0 || choice > 2)
                {
                    throw new Exception();
                }
            }
            catch(Exception)
            {
                Console.WriteLine("Invalid Gender Choice");
                return GetGender();
            }

            Gender genderType = 0 ;

            switch(choice)
            {
                case 0:
                    genderType = Gender.Male;
                    break;                  

                case 1:
                    genderType = Gender.Female;
                    break;

                case 2:
                    genderType = Gender.Other;
                    break;

            }

            return genderType;
        }


        public EmployeeType GetEmployeeType()
        {
            Console.WriteLine("Please Enter Employee Type ");
            Console.WriteLine("Enter 0 for PermanentEmployee ");
            Console.WriteLine(" Enter 1 for ContractualEmployee ");

            int choice = 0;
            try
            {
                choice = Convert.ToInt32(Console.ReadLine());
                if (choice < 0 || choice > 1)
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid Employee Type");
                return GetEmployeeType();
            }


            if (choice == 0)
                return EmployeeType.PermanentEmployee;

            else
                return EmployeeType.ContractualEmployee;


        }

    }
}

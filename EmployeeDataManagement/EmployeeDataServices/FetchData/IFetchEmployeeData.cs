﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDataManagement
{
    interface IFetchEmployeeData
    {
        void FetchAllEmployeeDetails(List<Employee> employeeList);

        void FetchEmployeeDetailsByEmailAddress(MailAddress mail, List<Employee> employeeList);

        void FetchEmployeeSalary(MailAddress mail, List<Employee> employeeList);

    }
}

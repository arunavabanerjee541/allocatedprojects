﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDataManagement
{
    class FetchEmployeeData : IFetchEmployeeData
    {
        public void FetchAllEmployeeDetails(List<Employee> employeeList)
        {
           foreach(Employee employee in employeeList)
            {
                Console.WriteLine(employee.FirstName + "  " + employee.LastName + "  " + employee.GetEmailAddress().Address + "  " + employee.GetGender() + "  " + employee.Salary + "  " + employee.GetEmployeeType());
                                  
            }
        }


        public void FetchEmployeeDetailsByEmailAddress(MailAddress mail, List<Employee> employeeList)
        {
            var employee = employeeList.Where(emp => emp.GetEmailAddress().Equals(mail)).Single();

         Console.WriteLine(employee.FirstName + "  " 
                           + employee.LastName + "  "
                           + employee.GetGender()+ "  "
                           + employee.Age + "  " 
                           + employee.GetEmailAddress()+ "  "
                           + employee.Salary + "  " 
                           + employee.GetEmployeeType());                                       

        }



        public void FetchEmployeeSalary(MailAddress mail, List<Employee> employeeList)
        {
            var employee = employeeList.Where(myEmployee => myEmployee.GetEmailAddress().Equals(mail)).Single();

            employee.CalculateSalary(employee.Salary);
            
        }

    }
}

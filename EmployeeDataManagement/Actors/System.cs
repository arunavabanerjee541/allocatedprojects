﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDataManagement
{
   public class System
    {
        public Manager MyManager { get; set; }
        private List<Employee> _employeeList;
       
        public System()
        {
            MyManager = new Manager();
            _employeeList = new List<Employee>();
        }

        public List<Employee> GetEmployeeList()
        {
            return _employeeList;
        }

        public OperationType ShowAvailableOptions()
        {
           return MyManager.ShowOptions();
        }   

    }
}

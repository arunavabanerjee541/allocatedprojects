﻿using System.Collections.Generic;
using System.Net.Mail;
using System;

namespace EmployeeDataManagement
{
    public class Manager : PermanentEmployee
    {
        private IManipulateEmployeeData _manipulateData;
        private IFetchEmployeeData _fetchData;
        private IValidate _validate;
        private ValidationHelper _validationHelper;

        public Manager()
        {
            _manipulateData = new ManipulateEmployeeData();
            _fetchData = new FetchEmployeeData();
            _validate = new Validate();
            _validationHelper = new ValidationHelper();
        }
        

        public void AddAnEmployee(List<Employee> employeeList)
        {
            _manipulateData.AddEmployeeData(employeeList);

        }

        public void GetAllEmployeeDetails(List<Employee> employeeList)
        {
            _fetchData.FetchAllEmployeeDetails(employeeList);

        }

         public void GetSpecificEmployeeByEmailAddress(List<Employee> employeeList)
         {
            Console.WriteLine(" Please enter employee email address ");
            string mail = Console.ReadLine();

            if (_validate.IsDataValid(mail, _validationHelper.DoesMailAddressExist,employeeList))
                _fetchData.FetchEmployeeDetailsByEmailAddress(new MailAddress(mail),employeeList);
            
             else
                Console.WriteLine("Employee not Found ");

        }


        public void UpdateAnEmployee(List<Employee> employeeList)
        {
            _manipulateData.UpdateEmployeeData(employeeList);

        }

        public void DeleteEmployeeByMail(List<Employee> employeeList)
        {
            Console.WriteLine("Please enter Employee email address");
            string mail = Console.ReadLine();
          if( _validate.IsDataValid(mail, _validationHelper.DoesMailAddressExist, employeeList))
            {
                _manipulateData.DeleteAnEmployee(new MailAddress(mail),employeeList);
            }
          else
                Console.WriteLine("No Employee Details Present ");

        }

        public void DeleteEmployeeByName(List<Employee> employeeList)
        {
            Console.WriteLine("Please enter Employee FirstName");
            string firstName = Console.ReadLine();
            if (_validate.IsDataValid(firstName, _validationHelper.DoesEmployeeWithThisNameExists, employeeList))       
                _manipulateData.DeleteAnEmployee(firstName,employeeList);
            
            else
                Console.WriteLine("Employee not Found " );

        }


        public OperationType ShowOptions()
        {
            Console.WriteLine("Please Select The Options ");
            Console.WriteLine("Press 1 to add an employee");
            Console.WriteLine("Press 2 to get details of all Employees");
            Console.WriteLine("Press 3 to update an employee ");
            Console.WriteLine("Press 4 to delete an employee by entering Employee address ");
            Console.WriteLine("Press 5 to delete an employee by entering employee name");
            Console.WriteLine("Press 6 to get details of specific Employee by entering Employee address ");
            Console.WriteLine("Press 7 to get total salary of an employee");
            Console.WriteLine(" Press any other key to exit ");

            int choice = 0;

            try
            {
                choice = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception)
            {
                Console.WriteLine(" INVALID CHOICE ");
                return ShowOptions();
            }

            if(choice <1 || choice > 7)
            {
                Environment.Exit(0);
            }

            return (OperationType)choice-1;


        }



        public void GetTotalSalaryByEmailAddress(List<Employee> employeeList)
        {
            Console.WriteLine("please enter employee email address");
            string mail = Console.ReadLine();
            if (_validate.IsDataValid(mail, _validationHelper.DoesMailAddressExist, employeeList))
                _fetchData.FetchEmployeeSalary(new MailAddress(mail),employeeList);
            else
            Console.WriteLine(" Employee not found ");

            
        }


    }
}
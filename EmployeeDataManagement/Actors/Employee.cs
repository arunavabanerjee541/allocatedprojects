﻿using System.Net.Mail;
namespace EmployeeDataManagement
{


    public abstract class Employee
    {
        protected  EmployeeType TypeOfEmployee;
        protected Gender Gender;
        protected MailAddress EmailAddress;
        public  string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public decimal Salary{ get; set; }
        
        public EmployeeType GetEmployeeType()
        {
            return TypeOfEmployee;
        }

        public void SetEmployeeType(EmployeeType employeeType)
        {
            TypeOfEmployee = employeeType;
        }

        public Gender GetGender()
        {
            return Gender;
        }

        public void SetGender(Gender genderType)
        {
            Gender = genderType;
        }


        public MailAddress GetEmailAddress()
        {
            return EmailAddress;
        }

        public void SetEmailAddress(MailAddress mail)
        {
            EmailAddress = mail;
        }


        public Employee(string firstName, string lastName, int age, decimal salary, Gender genderType, EmployeeType typeOfEmployee,MailAddress mail)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            Salary = salary;
            Gender = genderType;
            TypeOfEmployee = typeOfEmployee;
            EmailAddress = mail;

        }

        public Employee()
        {

        }

        public abstract void CalculateSalary(decimal salary);


    }
}
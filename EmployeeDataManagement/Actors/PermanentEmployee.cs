﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDataManagement
{
    public class PermanentEmployee : Employee
    {

        public PermanentEmployee(string firstName, string lastName, int age, decimal salary, Gender genderType, EmployeeType typeOfEmployee,MailAddress mail)
            : base(firstName,lastName,age,salary,genderType,typeOfEmployee,mail)
           {
           

           }

        public PermanentEmployee()
        {

        }



        public override void CalculateSalary(decimal salary)
          {
            decimal annualSalary = (salary * 12) + salary * (8 / Convert.ToDecimal(100)) ;

            Console.WriteLine(annualSalary);         

          }
    }
}

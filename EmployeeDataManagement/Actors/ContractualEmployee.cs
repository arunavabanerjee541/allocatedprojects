﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDataManagement
{
    class ContractualEmployee : Employee
    {

        public ContractualEmployee(string firstName, string lastName, int age, decimal salary, Gender genderType, EmployeeType typeOfEmployee,MailAddress mail)
           : base(firstName, lastName, age, salary, genderType, typeOfEmployee,mail)
        {
        }

        public override void CalculateSalary(decimal salary)
        {
            decimal annualsalary = salary * 12;
            Console.WriteLine(annualsalary);
        }

    }
}

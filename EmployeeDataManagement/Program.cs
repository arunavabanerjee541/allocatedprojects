﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDataManagement
{
    class Program
    {
        static void Main(string[] args)
        {
             System mySystem = Factory.GetSystemObj();
             Manager manager = mySystem.MyManager; 
             List<Employee> listOfEmployees = mySystem.GetEmployeeList();
             bool validInput = true;
        
            while(validInput)
            { 
              OperationType selectedOperationType = mySystem.ShowAvailableOptions();             
                switch (selectedOperationType)
                {
                    case OperationType.AddAnEmployee:
                        manager.AddAnEmployee(listOfEmployees);
                        break;

                    case OperationType.GetDetailsOfAllEmployees:
                        manager.GetAllEmployeeDetails(listOfEmployees);
                        break;

                    case OperationType.UpdateAnEmployee:
                        manager.UpdateAnEmployee(listOfEmployees);
                        break;

                    case OperationType.DeleteEmployeeByEmail:
                        manager.DeleteEmployeeByMail(listOfEmployees);
                        break;

                    case OperationType.DeleteEmployeeByName:
                        manager.DeleteEmployeeByName(listOfEmployees);
                        break;

                    case OperationType.GetEmployeeByEmailAddress:
                        manager.GetSpecificEmployeeByEmailAddress(listOfEmployees);
                        break;

                    case OperationType.GetTotalSalary:
                        manager.GetTotalSalaryByEmailAddress(listOfEmployees);
                        break;                 

                }

            } 
          
        }
    }
}
